#import matplotlib.pyplot as plt
import csv
import numpy as np
import ncm as ncm

ncmValid = ncm.ncm


def checkValue(key,value,ncmID):
	#for index in range(len(list)):
	size = len(ncmID)#[index])
	#position = key.find(list[index])
	position = key[:size].find(ncmID)#[index])
	if(position >= 0): return float(value)
	
	return 0

def calcTotal(filename,index):
	total = 0
	with open(filename, 'rt') as f:
		reader = csv.reader(f,delimiter=';')
		for row in reader:
			total += checkValue(row[2],row[10],ncmValid[index])
	return total

def setFilenameImp(index):
	return'DataImp/IMP_'+str(index)+'.csv'

def setFilenameExp(index):
	return'DataExp/EXP_'+str(index)+'.csv'


ano = [[]]
totalImp = [[]]
totalExp = [[]]
for y in range(len(ncmValid)):
	print(ncmValid[y])
	for x in range(1997,2020):
		ano[y].append(x)
		totalImp[y].append(calcTotal(setFilenameImp(x),y))
		totalExp[y].append(calcTotal(setFilenameExp(x),y))
		print(ano[y][-1],totalImp[y][-1],totalExp[y][-1])
	ano.append([])
	totalImp.append([])
	totalExp.append([])

with open('output.csv', 'w') as csvfile:
	filewriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
	for grup in range(len(ncmValid)):
		filewriter.writerow([ncmValid[grup],ncm.ncmLabel[grup]])
		filewriter.writerow(['Ano', 'Importação','Exportação'])
		for linha in range (len(ano[grup])):
			filewriter.writerow([str(int(ano[grup][linha])),str(int(totalImp[grup][linha])),str(int(totalExp[grup][linha]))])




























